@extends('layouts.app')

@section('content')
    <div class="panel panel-default" align="left" style="width: 1000px">
        <div class="panel-heading"><h4>Articles</h4></div>
        <div class="panel-body">
            <div class="container" style="margin-top: 15px">


                @foreach($articles as $article)

                    <p style="margin-top: 30px; margin-left: 60px;">@markdown($article->title)</p>
                    <p> @markdown(str_limit($article->body, 50))</p>
                    <a class="btn btn-link btn-xs" href="{{route('articles.readArticle',$article)}}">Read more</a>
                    <hr align="left" width="50%">

                @endforeach


                <br><br><br>
                {{$articles->links()}}
                <br><br><br><br><br>
                @if(Auth::check())
                    <a href="{{route('articles.create')}}"><h2>Add new article</h2></a>
                @endif
            </div>
        </div>
    </div>

@endsection
