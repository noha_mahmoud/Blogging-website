@extends('layouts.app')

@section('content')
    <div class="panel panel-default" align="left" style="width: 1000px">
        <div class="panel-heading"><h4>@markdown($article->title)</h4></div>
        <div class="panel-body">
            <div class="container" style="margin-top: 15px">
                <div style="width: 700px;">@markdown($article->body)</div>

                <br>
                <label readonly>Likes: </label>
                <input type="text" style="border: none" readonly
                       value="{{$article->likesCounter >0 ? $article->likesCounter:'0'}}">
                <br>

                @if(auth()->user())
                    @can('update',$article)
                        <form action="{{ route('articles.edit', $article) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="GET">

                            <input class="btn btn-info btn-xs" type="submit" value="Edit">
                        </form>
                    @endcan
                    @can('delete',$article)
                        <br>
                        <form action="{{ route('articles.destroy', $article) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">

                            <input class="btn btn-info btn-xs" type="submit" value="Delete">
                        </form>
                    @endcan

                    @cannot('update',$article)
                        @if(\App\Models\Like::where('article_id', $article->id)->where('user_id', auth()->user()->id)->first())

                            @if(\App\Models\Like::where('article_id', $article->id)->where('user_id', auth()->user()->id)->where('type', 1)->first())
                                <a class="btn btn-default btn-xs " href="{{route('articles.like',$article)}}">Like</a>
                                <a class="btn btn-info btn-xs " href="{{route('articles.dislike',$article)}}">Dislike</a>
                            @else
                                <a class="btn btn-info btn-xs " href="{{route('articles.like',$article)}}">Like</a>
                                <a class="btn btn-default btn-xs "
                                   href="{{route('articles.dislike',$article)}}">Dislike</a>
                            @endif

                        @else
                            <a class="btn btn-info btn-xs" href="{{route('articles.like',$article)}}">Like</a>
                            <a class="btn btn-info btn-xs" href="{{route('articles.dislike',$article)}}">Dislike</a>
                        @endif
                    @endcannot
                @endif

            </div>
        </div>
    </div>

@endsection
