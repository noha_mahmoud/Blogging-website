@extends('layouts.app')

@section('content')
    <div class="panel panel-default" align="left">
        <div class="panel-heading"><h4>New article</h4></div>
        <div class="panel-body">
            <div class="container" style="margin-top: 15px">
                <form action="{{route('articles.store')}}" method="POST">
                    {{csrf_field()}}


                    <label><h4>Article title:</h4></label>
                    <input type="text" name="title" class="form-control" style="width: 650px;" required>
                    </br>
                    </br>
                    <label><h4>Article body:</h4></label>
                    <input type="text" name="body" class="form-control" style="width: 650px;" required>
                    </br>
                    </br>

                    <input class="btn btn-info btn-md" type="submit" value="Add">

                </form>
            </div>
        </div>
    </div>
@endsection
