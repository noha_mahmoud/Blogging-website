@extends('layouts.app')

@section('content')
    <div class="panel panel-default" align="left">
        <div class="panel-heading"><h4>Edit article</h4></div>
        <div class="panel-body">
            <div class="container" style="margin-top: 15px">

                <form action="{{route('articles.update',$article)}}" method="POST">
                    {{csrf_field()}}

                    <label><h4>Article title:</h4></label>
                    <input type="text" name="title" class="form-control" style="width: 650px;" value="{{$article->title}}" required>
                    </br>
                    </br>
                    <label><h4>Article body:</h4></label>
                    <input type="text" name="body" class="form-control" style="width: 650px;" value="{{$article->body}}" required>
                    </br>
                    </br>

                    <input type="hidden" name="_method" value="PUT">

                    <input class="btn btn-info btn-md" type="submit" value="Update">

                </form>
            </div>
        </div>
    </div>
@endsection
