@extends('layouts.app')

@section('content')
    <div class="panel panel-default" align="left" style="width: 1000px">
        @if($type==0)
            <div class="panel-heading"><h4>Most viewed articles</h4></div>
        @elseif($type==1)
            <div class="panel-heading"><h4>Most liked articles</h4></div>
        @else
            <div class="panel-heading"><h4>Recommended articles</h4></div>
        @endif


        <div class="panel-body">
            <div class="container" style="margin-top: 15px">


                @foreach($articles as $article)

                    @markdown($article->title)
                    <p>@markdown(str_limit($article->body,50))</p>
                    <a href="{{route('articles.readArticle',$article)}}">Read more</a>
                    <br>
                    <label readonly>Likes: </label>
                    <input type="text" style="border: none" readonly value="{{$article->likesCounter >0 ? $article->likesCounter:'0'}}">
                    <label readonly>Views: </label>
                    <input type="text" style="border: none" readonly value="{{$article->viewsCounter }}">
                    <br>
                    <hr align="left" width="50%">

                @endforeach
                <br><br><br>
                {{$articles->links()}}
                <br><br><br><br><br>
                @if(Auth::check())
                    <a href="{{route('articles.create')}}"><h2>Add new article</h2></a>
                @endif

            </div>
        </div>
    </div>
@endsection