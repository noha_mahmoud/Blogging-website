@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading"><h4>Welcome {{ Auth::user()->name }} you are logged in!</h4></div>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


        </div>
    </div>
@endsection
