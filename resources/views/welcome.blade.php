@extends('layouts.app')

@section('content')
        <div class="col-sm-8 text-left" >
            <div class="panel panel-default" align="left" style="width: 1000px">
                <div class="panel-heading"><h4 align="left">Top views</h4></div>
                <div class="panel-body">
                    <div class="container" style="margin-top: 15px">
                        @foreach($articles1 as $article)

                            @markdown($article->title)
                            <p width="700px;">@markdown(str_limit($article->body, $limit = 50, $end = ' ...'))</p>
                            <a href="{{route('articles.readArticle',$article)}}">Read more</a>
                            <br>
                            <label readonly>Likes: </label>
                            <input type="text" style="border: none" readonly value="{{$article->likesCounter>0?$article->likesCounter:0}}">
                            <label readonly>Views: </label>
                            <input type="text" style="border: none" readonly value="{{$article->viewsCounter }}">
                            <br>
                            <hr align="left" width="50%">

                        @endforeach
                            <a href="{{route('ordering.mostViewed')}}" >See all</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8 text-left">
            <div class="panel panel-default" align="left" style="width: 1000px">
                <div class="panel-heading"><h4>Most liked</h4></div>
                <div class="panel-body">
                    <div class="container" style="margin-top: 15px">
                        @foreach($articles2 as $article)

                            @markdown($article->title)
                            <p width="700px;">@markdown(str_limit($article->body,50))</p>
                            <a href="{{route('articles.readArticle',$article)}}">Read more</a>
                            <br>
                            <label readonly>Likes: </label>
                            <input type="text" style="border: none" readonly value="{{$article->likesCounter>0?$article->likesCounter:0}}">
                            <label readonly>Views: </label>
                            <input type="text" style="border: none" readonly value="{{$article->viewsCounter }}">
                            <br>
                            <hr align="left" width="50%">

                        @endforeach
                            <a href="{{route('ordering.mostLiked')}}" >See all</a>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8 text-left">
            <div class="panel panel-default" align="left" style="width: 1000px">
                <div class="panel-heading"><h4>Recommended</h4></div>
                <div class="panel-body">
                    <div class="container" style="margin-top: 15px">
                        @foreach($articles3 as $article)

                            @markdown($article->title)
                            <p width="700px;">@markdown(str_limit($article->body, $limit = 50, $end = ' ...'))</p>
                            <a href="{{route('articles.readArticle',$article)}}">Read more</a>
                            <br>
                            <label readonly>Likes: </label>
                            <input type="text" style="border: none" readonly value="{{$article->likesCounter>0?$article->likesCounter:0}}">
                            <label readonly>Views: </label>
                            <input type="text" style="border: none" readonly value="{{$article->viewsCounter }}">
                            <br>
                            <hr align="left" width="50%">

                        @endforeach
                            <a href="{{route('ordering.recommended')}}" >See all</a>

                    </div>
                </div>
            </div>
        </div>



        </div>
@endsection

