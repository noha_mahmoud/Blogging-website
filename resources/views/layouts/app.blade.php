<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}>
<head>
    <title>BLogging website</title>
    <meta charset=" utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
        margin-bottom: 0;
        border-radius: 0;
    }

    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {
        height: 100%;
    }

    /* Set gray background color and 100% height */
    .sidenav {
        padding-top: 20px;
        height: 100%;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
        .sidenav {
            height: auto;
            padding: 15px;
        }

        .row.content {
            height: auto;
        }
    }
</style>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                Blogging website </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a class="btn btn-link" href="{{route('home')}}">Home</a></li>
            <li style="margin-top: 8px;">
                <div class="dropdown">


                    <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">Articles
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a class="btn btn-link" href="{{route('articles.index')}}">All articles</a></li>
                        <li><a class="btn btn-link" href="{{route('ordering.mostViewed')}}">Most viewed articles</a></li>
                        <li><a class="btn btn-link" href="{{route('ordering.mostLiked')}}">Most liked articles</a></li>
                        <li><a class="btn btn-link" href="{{route('ordering.recommended')}}">Recommended articles</a></li>
                    </ul>
                </div>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @guest
                <li><a  href="{{ route('login') }}">Login</a></li>
                <li><a  href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
        </ul>
    </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-8" style="margin-top: 20px">
            @yield('content')
        </div>
    </div>
</div>


</body>
</html>
