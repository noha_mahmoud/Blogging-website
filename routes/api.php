<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');
Route::resource('articles', 'ArticlesController');
Route::post('like/{article}', 'LikeController@like')->name('Api.articles.like')->middleware('auth:api');
Route::post('dislike/{article}', 'LikeController@dislike')->name('Api.articles.dislike')->middleware('auth:api');
Route::get('orderingViewed', 'ArticlesController@mostViewed')->name('Api.ordering.mostViewed');
Route::get('orderingLiked', 'ArticlesController@mostLiked')->name('Api.ordering.mostLiked');
Route::get('orderingRecommended', 'ArticlesController@recommended')->name('Api.ordering.recommended');


