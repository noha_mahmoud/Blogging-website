<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@mainOrdering');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::Resource('/articles', 'ArticlesController');
Route::get('/orderingViewed', 'ArticlesController@mostViewed')->name('ordering.mostViewed');
Route::get('/orderingLiked', 'ArticlesController@mostLiked')->name('ordering.mostLiked');
Route::get('/orderingRecommended', 'ArticlesController@recommended')->name('ordering.recommended');
Route::get('/articles/{article}', 'ArticlesController@readArticle')->name('articles.readArticle');

Route::get('/like/{article}', 'LikeController@like')->name('articles.like')->middleware('auth');
Route::get('/dislike/{article}', 'LikeController@dislike')->name('articles.dislike')->middleware('auth');
