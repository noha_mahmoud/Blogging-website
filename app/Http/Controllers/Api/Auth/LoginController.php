<?php

namespace App\Http\Controllers\api\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Login to the application, returns token.
     *
     * @param Request $request
     * @return UserResource|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        //
        if ($errorResponse = $this->attemptUser($request)) {
            return $errorResponse;
        }

        return $this->generateToken($request);
    }

    /**
     * check user email and password.
     *
     * @param Request $request
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function attemptUser(Request $request)
    {
        $this->validateRequest($request);
        $credentials = $this->credentials($request);

        // Check user's main information
        if (! Auth::attempt($credentials)) {
            return response([
                'errors' => [
                    $this->username() => [trans('auth.failed')],
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }

        return null;
    }

    /**
     * Check if user is activated.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|null|\Symfony\Component\HttpFoundation\Response
     */
    public function accountVerified(Request $request)
    {
        $user = $request->user();

        if (! $user->is_active) {
            return new UserResource($user);
        }

        return $this->generateToken($request);
    }

    /**
     * To generate login token.
     *
     * @param Request $request
     * @return UserResource
     */
    public function generateToken(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $accessToken = $user->createToken(null)->accessToken;

        return (new UserResource($user))->additional([
            'token' => $accessToken,
        ]);
    }

    /**
     * Get the credentials main user information for the login request.
     *
     * @param $request
     *
     * @return array
     */
    private function credentials($request)
    {
        return [
            $this->username() => $request->input($this->username()),
            'password' => $request->input('password'),
        ];
    }

    /**
     * Get the username field in the database.
     *
     * @return string
     */
    protected function username()
    {
        return 'email';
    }

    /**
     * Validate the login request.
     *
     * @param $request
     */
    private function validateRequest($request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'password' => 'required',
        ]);
    }

}
