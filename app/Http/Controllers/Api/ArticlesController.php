<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Resources\ArticleResource;
use Symfony\Component\HttpFoundation\Response;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return ArticleResource::collection(Article::paginate(2));
    }

    public function show(Article $article)
    {
        if (auth()->guest() || (auth()->id() != $article->user_id)) {
            $article->calculateViews();
            $article->save();
        }

        return new ArticleResource($article);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'body' => 'required|string',
        ]);
        $article = $request->user()->articles()->create($request->all());



        return new ArticleResource($article);
    }

    public function update(Request $request, Article $article)
    {

        $this->authorize('update', $article);
        $article->update($request->all());

        return new ArticleResource($article);
    }

    public function destroy(Article $article)
    {

        $this->authorize('delete', $article);
        $article->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function mostViewed()
    {
        $articles = Article::latest('viewsCounter')->paginate(3);

        return ArticleResource::collection(Article::paginate(2));
    }

    public function mostLiked()
    {
        $articles = Article::latest('likesCounter')->paginate(3);

        return ArticleResource::collection(Article::paginate(2));
    }

    public function recommended()
    {
        $articles = Article::latest('weight')->paginate(3);

        return ArticleResource::collection(Article::paginate(2));
    }
}
