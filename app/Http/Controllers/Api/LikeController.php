<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use Symfony\Component\HttpFoundation\Response;

class LikeController extends Controller
{
    //
    public function like($id)
    {
       $article = Article::find($id);
        $article->like();
        return response(null,Response::HTTP_OK);

    }

    public function dislike($id)
    {
        $article = Article::find($id);
        $article->dislike();
        return response(null,Response::HTTP_OK);

    }
}
