<?php

namespace App\Http\Controllers\Web;

use App\Models\Article;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /*public function __construct()
    {
        $this->middleware('auth');
    }*/



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function mainOrdering()
    {
        $articles1 = Article::latest('viewsCounter')->limit(3)->get();
        $articles2 = Article::latest('likesCounter')->limit(3)->get();
        $articles3 = Article::latest('weight')->limit(3)->get();

        return view('welcome', compact('articles1', 'articles2', 'articles3'));


    }
}
