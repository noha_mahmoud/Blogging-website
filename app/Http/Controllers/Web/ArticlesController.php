<?php

namespace App\Http\Controllers\Web;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('store', 'create', 'edit', 'update', 'destroy');
    }

    public function create()
    {
        return view('BloggingViews.addArticle');
    }

    public function index()
    {
        $articles = Article::paginate(3);

        return view('BloggingViews.viewArticle', compact('articles'));
    }

    public function readArticle(Article $article)
    {
        if (auth()->guest() || (auth()->id() != $article->user_id)) {
            $article->calculateViews();
            $article->save();
        }

        return view('BloggingViews.showArticle', compact('article'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'body' => 'required|string',
        ]);
        $request->user()->articles()->create($request->all());


        return redirect()->route('articles.index');
    }

    public function edit(Article $article)
    {
        $this->authorize('update', $article);

        return view('BloggingViews.editArticle', compact('article'));
    }

    public function update(Request $request, Article $article)
    {
        $this->authorize('update', $article);
        $article->update($request->all());

        return redirect()->route('articles.readArticle', compact('article'));
    }

    public function destroy(Article $article)
    {
        $this->authorize('delete', $article);
        $article->delete();

        return redirect()->route('articles.index');
    }

    public function mostViewed()
    {
        $type = 0;
        $articles = Article::latest('viewsCounter')->paginate(3);

        return view('BloggingViews.ordering', compact('articles', 'type'));
    }

    public function mostLiked()
    {
        $type = 1;
        $articles = Article::latest('likesCounter')->paginate(3);

        return view('BloggingViews.ordering', compact('articles', 'type'));
    }

    public function recommended()
    {
        $type = 2;
        $articles = Article::latest('weight')->paginate(3);

        return view('BloggingViews.ordering', compact('articles', 'type'));
    }
}
