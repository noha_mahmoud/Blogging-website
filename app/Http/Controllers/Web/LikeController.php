<?php

namespace App\Http\Controllers\Web;

use App\Models\Article;


class LikeController extends Controller
{
    public function like(Article $article)
    {
        $article->like();
        return redirect()->back();

    }

    public function dislike(Article $article)
    {
        $article->dislike();
        return redirect()->back();


    }
}
