<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ArticleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'title' => $this->title,
                'body' => $this->body,
                'likes' => $this->when($this->likesCounter > 0, $this->likesCounter),
                'views' => $this->when($this->viewsCounter > 0, $this->viewsCounter),
                'user' => new UserResource($this->user),
                'links' => $this->links($request)
            ];
    }


    private function links($request)
    {
        $user = $request->user();
        return  [
            'update' => $this->when($user->can('update', $this->resource), [
                'link' => route('articles.update', $this->resource),
                'method' => 'PATCH'
            ]),

            'delete' => $this->when($user->can('delete', $this->resource),[
                'link' => route('articles.destroy', $this->resource),
                'method' => 'DELETE'
            ]),
            'like' => $this->when($user->can('like', $this->resource),[
                'link' => route('Api.articles.like', $this->resource),
                'method' => 'POST'
            ]),
            'dislike' => $this->when($user->can('dislike', $this->resource),[
                'link' => route('Api.articles.dislike', $this->resource),
                'method' => 'POST'
            ])
        ];
    }
}
