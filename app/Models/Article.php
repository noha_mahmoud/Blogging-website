<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Article extends Model
{
    public $table = "articles";

    protected $fillable = [
        'title', 'body'
    ];

    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function calculateLikes()
    {
        $this->likesCounter = $this->likes()->sum('type');
    }

    public function calculateWeight()
    {
        if($this->viewsCounter>0)
        {
            $this->weight = $this->likesCounter / $this->viewsCounter ;
        }

    }

    public function calculateViews()
    {
        $this->viewsCounter++;
        if ($this->viewsCounter > 0) {
            $this->calculateWeight();
        }
    }

    public function like()
    {
        $this->vote(1);
    }

    public function dislike()
    {
        $this->vote(-1);
    }

    public function vote($type)
    {

        $like = $this->likes()->firstOrNew(['user_id' => auth()->user()->id]);
        if($like->exists && $like->type==$type){
            $like->delete();
        }
        else{
            $like->type=$type;
            $like->save();
        }

        $this->calculateLikes();
        $this->calculateWeight();
        $this->save();

        return $this;
    }

}
